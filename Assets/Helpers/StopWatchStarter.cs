﻿using UnityEngine;
using System.Collections;

public class StopWatchStarter : MonoBehaviour 
{	
    void Update() 
    {
        GetComponent<PerformanceTester>().StartTimer();
	}
}
