﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(InterfacedComponent))]
public class BaseComponent : PooledBehaviour
{	
    Matrix4x4 A, B;
    public float ax = 10f;
    public float ay = 11f;

    float mx = 16f;
    float my = 17f;

    InterfacedComponent ic;

    public override void Awake()
    {
        base.Awake();
        ic = GetComponent<InterfacedComponent>();
    }

    public virtual void VirtualEmptyMethod()
    {
    }

    public void EmptyMethod()
    {
    }

    public override void Register() { BehaviourPool<BaseComponent>.Instance().Register(this); }
    public override void Unregister() { BehaviourPool<BaseComponent>.Instance().Unregister(this); }

    public void AddToLocal()
    {
        ax = ic.ix + ic.iy;
        ay = ic.ix + ic.iy;
    }

    public void AddToLocalViaInterface()
    {
        ax = ic.posX + ic.posY;
        ay = ic.posX + ic.posY;
    }

    public void AddToRemote()
    {
        ic.ix = mx + my;
        ic.iy = mx + my;
    }

    public void AddToRemoteViaInterface()
    {
        ic.posX = mx + my;
        ic.posY = mx + my;
    }
}
