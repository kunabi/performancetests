﻿using UnityEngine;
using System.Collections;

interface IMethodInterface { void EmptyInterfaceMethod(); }

public class DerivedComponent : BaseComponent, IMethodInterface
{
    public float bx = 5f;
    public float by = 7f;

    public override void VirtualEmptyMethod() { }

    public void EmptyInterfaceMethod() { }

    public override void Register() { BehaviourPool<DerivedComponent>.Instance().Register(this); }
    public override void Unregister() { BehaviourPool<DerivedComponent>.Instance().Unregister(this); }
}
