﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SwitchScene : MonoBehaviour 
{
    public void OnPrevSceneButtonPressed()
    {
        int nextIndex = SceneManager.GetActiveScene().buildIndex - 1;
        if (nextIndex < 0)
            nextIndex = SceneManager.sceneCountInBuildSettings - 1;
        SceneManager.LoadScene(nextIndex);
    }

    public void OnNextSceneButtonPressed()
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
    }
}
