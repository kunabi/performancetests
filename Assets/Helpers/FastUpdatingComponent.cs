﻿using UnityEngine;
using System.Collections;

public class FastUpdatingComponent : PooledBehaviour
{	
    Matrix4x4 A, B;

    public override void FastUpdate () 
    {
    }

    public override void Register() { BehaviourPool<FastUpdatingComponent>.Instance().Register(this); }
    public override void Unregister() { BehaviourPool<FastUpdatingComponent>.Instance().Unregister(this); }
}
