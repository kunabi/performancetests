﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System.Diagnostics;


public abstract class PooledBehaviour : MonoBehaviour
{
    public int poolIndex = -1;
    public virtual void Awake() { Register(); }
    public virtual void OnEnable() { if (poolIndex == -1) Register(); }
    public virtual void OnDestroy() { if (poolIndex > -1) Unregister(); }
    public virtual void OnDisable() { if (poolIndex > -1) Unregister(); }
    public abstract void Register();
    public abstract void Unregister();

    public virtual void FastUpdate() {}
    public virtual void FastFixedUpdate() {}
}


public class BehaviourPool<T> : Singleton<BehaviourPool<T>>
    where T : PooledBehaviour
{
    public T[] pool = new T[500];
    public int poolSize;

    public Stopwatch stopWatch = new Stopwatch();

    public BehaviourPool()
    {
        if (typeof(T).GetMethod("FastUpdate").DeclaringType == typeof(T))
            UpdateEvent.Instance().OnUpdate += HandleUpdate;
        if (typeof(T).GetMethod("FastFixedUpdate").DeclaringType == typeof(T))
            UpdateEvent.Instance().OnFixedUpdate += HandleFixedUpdate;
    }

    public void Register(T component)
    {
        if (poolSize >= pool.Length)
            System.Array.Resize(ref pool, pool.Length * 2);

        pool[poolSize] = component;
        component.poolIndex = poolSize;
        poolSize++;
    }

    public void Unregister(T component)
    {
        if (poolSize > component.poolIndex)
            return;
        
        int index = component.poolIndex;
//        Assert.IsTrue(index > -1 && index < poolSize);
        poolSize--;
        component.poolIndex = -1;
        pool[index] = pool[poolSize];
        pool[index].poolIndex = index;
    }

    void HandleUpdate()
    {
        stopWatch.Start();
        for (int i = 0; i < poolSize; i++)
            pool[i].FastUpdate();
        stopWatch.Stop();
    }

    void HandleFixedUpdate()
    {
        for (int i = 0; i < poolSize; i++)
            pool[i].FastFixedUpdate();
    }
}

public class UpdateEvent : SingletonBehaviour<UpdateEvent>
{
    public event System.Action OnUpdate;
    public event System.Action OnFixedUpdate;

    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (OnUpdate != null)
            OnUpdate();
    }

    void FixedUpdate()
    {
        if (OnFixedUpdate != null)
            OnFixedUpdate();
    }
}

public class SingletonBehaviour<T> : MonoBehaviour
    where T : Component
{
    protected static T _instance = null;

    public static T Instance()
    {
        if (!_instance)
        {
            _instance = FindObjectOfType<T>();
            if (!_instance)
            {
                GameObject go = new GameObject(typeof(T).Name);
                _instance = go.AddComponent<T>();
            }
        }
        return _instance;
    }
}


public class Singleton<T>
    where T : class, new()
{
    static T _instance = null;

    public static T Instance()
    {
        if (_instance == null)
            _instance = new T();
        return _instance;
    }
}