﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;


public abstract class PerformanceTester : PooledBehaviour 
{
    public string description;

    protected string formatString = "{0:N2} ms";
    protected Text timeDisplay;
    protected Stopwatch sw = new Stopwatch();
    float currentWatch;

    const int BufferSize = 250;
    int bufferIndex;
    double[] time = new double[BufferSize];
    double elapsed;

    public override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 60;

        Transform textObject = transform.FindChild("Time");
        if (textObject != null)
            timeDisplay = textObject.GetComponent<Text>();

        SetDescription(description);
    }


    protected void SetDescription(string desc)
    {
        Transform textObject = transform.FindChild("Info");
        if (textObject != null)
        {
            Text infoDisplay = textObject.GetComponent<Text>();
            if (infoDisplay != null)
                infoDisplay.text = desc;
        }
    }

    public override void FastUpdate()
    {
        StartTimer();
        ProfileCode();
        StopTimerAndUpdateDisplay(sw);
    }

    public void StartTimer()
    {
//        currentWatch = Time.realtimeSinceStartup;
        sw.Start();
    }

    public void StopTimerAndUpdateDisplay(Stopwatch watch)
    {
        watch.Stop();
        elapsed -= time[bufferIndex];
        time[bufferIndex] = watch.Elapsed.TotalMilliseconds / (float)BufferSize;
//        time[bufferIndex] = (Time.realtimeSinceStartup - currentWatch) / (float)BufferSize;
        elapsed += time[bufferIndex];
        bufferIndex = (bufferIndex+1) % BufferSize;

        if (timeDisplay != null)
            timeDisplay.text = string.Format(formatString, elapsed);

        watch.Reset();
    }

    public virtual void ProfileCode() {}

    public override void Register() { BehaviourPool<PerformanceTester>.Instance().Register(this); }
    public override void Unregister() { BehaviourPool<PerformanceTester>.Instance().Unregister(this); }
}
