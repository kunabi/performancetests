﻿using UnityEngine;
using System.Collections;

public class PrefabSpawner : MonoBehaviour 
{
    public Object prefab;
    public int count;

	// Use this for initialization
	void Awake () 
    {
        for (int i = 0; i < count; i++)
            GameObject.Instantiate(prefab);
    }
}
