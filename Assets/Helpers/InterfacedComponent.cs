﻿using UnityEngine;
using System.Collections;


public interface ISpatialDataProvider
{
    float posX { get; }
    float posY { get; }
}

public class InterfacedComponent : PooledBehaviour, ISpatialDataProvider 
{
    Matrix4x4 A;

    public float posX { get; set; }
    public float posY { get; set; }

    public float autoGetterSetter { get; set; }
    float _normalProp;
    public float normalGetterSetter { get { return _normalProp; } set { _normalProp = value; } }

    public float ix = 23f, iy = 42f;

    void Start()
    {
        posX = 19f;
        posY = 20f;

        _normalProp = 23f;
        autoGetterSetter = 545f;
    }

    public override void Register() { BehaviourPool<InterfacedComponent>.Instance().Register(this); }
    public override void Unregister() { BehaviourPool<InterfacedComponent>.Instance().Unregister(this); }
}
