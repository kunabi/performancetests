﻿using UnityEngine;
using System.Collections;

public class StopWatchStopper : PerformanceTester 
{
	public override void FastUpdate () {}

    void Update()
    {
        StopTimerAndUpdateDisplay(sw);
    }
}
