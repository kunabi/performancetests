﻿using UnityEngine;
using System.Collections;


public class DotManual : PerformanceTester
{
    Vector2 A = new Vector2(2f,3f), B = new Vector2(4f,5f);
    public float dot;

    public override void Awake()
    {
        description = "100k Vector2 dot products (without using Vector2.Dot)";
        base.Awake();
    }

    public override void ProfileCode()  
    {
        float a = 0f;
        for (int i = 0; i < 100000; i++)
            a += A.x * B.x + A.y * B.y;
        dot = a;
    }
}
