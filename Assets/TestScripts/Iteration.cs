﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


class TestObject
{
    float A;
//    Matrix4x4 A;
//    Matrix4x4 B;
}


public class Iteration : PerformanceTester 
{
    const int numItems = 10000;
    TestObject[] array = new TestObject[numItems];
    LinkedList<TestObject> linkedList = new LinkedList<TestObject>();
    HashSet<TestObject> hashSet = new HashSet<TestObject>();

    public enum Target { Array, LinkedList, HashSet };
    public Target target;

	public override void Awake () 
    {
        for (int i = 0; i < numItems; i++)
        {
            if (target == Target.Array)
                array[i] = new TestObject();
            else if (target == Target.LinkedList)
                linkedList.AddLast(new TestObject());
            else
                hashSet.Add(new TestObject());
        }   
        description = "Iteration over " + target + " of " + numItems + " objects";
        base.Awake();
	}
	
	// Update is called once per frame
	public override void FastUpdate () 
    {
        TestObject obj;
        if (target == Target.Array)
        {
            StartTimer();
            for (int i = 0; i < numItems; i++)
                obj = array[i];
            StopTimerAndUpdateDisplay(sw);
        }
        else if (target == Target.LinkedList)
        {
            StartTimer();
            for (LinkedList<TestObject>.Enumerator entry = linkedList.GetEnumerator(); entry.MoveNext();)
                obj = entry.Current;
            StopTimerAndUpdateDisplay(sw);
        }
        else
        {
            StartTimer();
            for (HashSet<TestObject>.Enumerator entry = hashSet.GetEnumerator(); entry.MoveNext();)
                obj = entry.Current;
            StopTimerAndUpdateDisplay(sw);
        }
	}
}
