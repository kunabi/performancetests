﻿using UnityEngine;
using System.Collections;

public class CallInterfaceAutoSetter : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " interface auto setter.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
        {
            result += i;
            components[i].posY = result;
        }
	}
}
