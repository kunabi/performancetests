﻿using UnityEngine;
using System.Collections;

public class CallNormalSetter : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " normal setter.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
        {
            result += i;
            components[i].normalGetterSetter = result;
        }
	}
}
