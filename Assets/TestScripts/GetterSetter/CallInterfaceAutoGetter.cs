﻿using UnityEngine;
using System.Collections;

public class CallInterfaceAutoGetter : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " interface auto getter.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
            result += components[i].posX;
	}
}
