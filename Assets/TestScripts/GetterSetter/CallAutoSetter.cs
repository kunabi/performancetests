﻿using UnityEngine;
using System.Collections;

public class CallAutoSetter : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " auto setter.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
        {
            result += i;
            components[i].autoGetterSetter = result;
        }
	}
}
