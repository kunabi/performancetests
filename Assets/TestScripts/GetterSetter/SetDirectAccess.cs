﻿using UnityEngine;
using System.Collections;

public class SetDirectAccess : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Setting " + count + " properties directly.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
        {
            result += i;
            components[i].iy = result;
        }
	}
}
