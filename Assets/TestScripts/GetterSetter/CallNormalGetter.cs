﻿using UnityEngine;
using System.Collections;

public class CallNormalGetter : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " normal getter.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
            result += components[i].normalGetterSetter;
	}
}
