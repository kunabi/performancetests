﻿using UnityEngine;
using System.Collections;

public class GetDirectAccess : GetterSetterCalls
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Reading " + count + " properties directly.");
    }

	public override void ProfileCode() 
    {
        result = 0f;
        for (int i = 0; i < count; i++)
            result += components[i].ix;
	}
}
