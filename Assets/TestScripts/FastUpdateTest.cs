﻿using UnityEngine;
using System.Collections;

public class FastUpdateTest : PerformanceTester 
{
    public override void Awake()
    {
        base.Awake();
    }

    public override void FastUpdate()
    {
        StopTimerAndUpdateDisplay( BehaviourPool<FastUpdatingComponent>.Instance().stopWatch );
    }
}
