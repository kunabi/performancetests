﻿using UnityEngine;
using System.Collections;

public class CallBaseMethod : MethodCalls 
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " empty base methods.");
    }

	public override void ProfileCode() 
    {
        for (int i = 0; i < count; i++)
            dcs[i].EmptyMethod();
	}
}
