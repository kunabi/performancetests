﻿using UnityEngine;
using System.Collections;


public class CreateDelegateToBaseMethod : MethodCalls 
{

    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " delegates, created each time before the call.");
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < count; i++)
        {
            System.Action dlg = () => dcs[i].EmptyMethod();
            dlg();
        }
	}
}
