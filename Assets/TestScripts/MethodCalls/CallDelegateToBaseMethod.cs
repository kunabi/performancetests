﻿using UnityEngine;
using System.Collections;

public class CallDelegateToBaseMethod : MethodCalls 
{
    System.Action[] actions;

    public override void Awake()
    {
        base.Awake();
        actions = new System.Action[count];
        for (int i = 0; i < count; i++)
            actions[i] = dcs[i].EmptyMethod;
        
        SetDescription("Calling " + count + " cached delegates to base methods.");
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < count; i++)
            actions[i]();
    }
}
