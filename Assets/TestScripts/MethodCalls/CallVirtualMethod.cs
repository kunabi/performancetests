﻿using UnityEngine;
using System.Collections;

public class CallVirtualMethod : MethodCalls 
{
    public override void Awake()
    {
        base.Awake();
        SetDescription("Calling " + count + " overriden methods.");
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < count; i++)
            dcs[i].VirtualEmptyMethod();
    }
}
