﻿using UnityEngine;
using System.Collections;

public class CallInterfaceMethod : MethodCalls 
{
    IMethodInterface[] imis;

    public override void Awake()
    {
        base.Awake();
        imis = dcs as IMethodInterface[];
        SetDescription("Calling " + count + " interface methods.");
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < count; i++)
            imis[i].EmptyInterfaceMethod();
    }
}
