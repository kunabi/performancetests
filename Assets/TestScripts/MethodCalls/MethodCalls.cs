﻿using UnityEngine;
using System.Collections;

public class MethodCalls : PerformanceTester 
{
    protected DerivedComponent[] dcs;
    protected int count;

    public override void Awake()
    {
        dcs = BehaviourPool<DerivedComponent>.Instance().pool;
        count = BehaviourPool<DerivedComponent>.Instance().poolSize;
        base.Awake();
    }
}

public class GetterSetterCalls : PerformanceTester 
{
    protected InterfacedComponent[] components;
    protected int count;
    protected float result = 10f;

    public override void Awake()
    {
        components = BehaviourPool<InterfacedComponent>.Instance().pool;
        count = BehaviourPool<InterfacedComponent>.Instance().poolSize;
        base.Awake();
    }
}