﻿using UnityEngine;
using System.Collections;

public class MagnitudeV2 : PerformanceTester 
{
    Vector2 v = new Vector2(5f,50f);
    public float len;
	
    public override void Awake()
    {
        description = "100k Vector2.magnitudes";
        base.Awake();
    }

    public override void ProfileCode() 
    {
        float a = 0f;
        for (int i = 0; i < 100000; i++)
            a += v.magnitude;
        len = a;
    }
}
