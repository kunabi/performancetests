﻿using UnityEngine;
using System.Collections;

public class MagnitudeSystemMath : PerformanceTester 
{
    Vector2 v = new Vector2(5f,50f);
    public float len;
	
    public override void Awake()
    {
        description = "100k Vector2.magnitudes, manually, using System.Math.Sqrt";
        formatString = "{0:N3} ms";
        base.Awake();
    }

    public override void ProfileCode() 
    {
        float a = 0f;
        for (int i = 0; i < 100000; i++)
            a += (float)System.Math.Sqrt(v.x * v.x + v.y * v.y);
        len = a;
    }
}
