﻿using UnityEngine;
using System.Collections;

public class MagnitudeMathf : PerformanceTester 
{
    Vector2 v = new Vector2(5f,50f);
    public float len;
	
    public override void Awake()
    {
        description = "100k Vector2.magnitudes, manually, with Mathf.Sqrt.";

        base.Awake();
    }

    public override void ProfileCode() 
    {
        float a = 0f;
        for (int i = 0; i < 100000; i++)
            a += Mathf.Sqrt(v.x * v.x + v.y * v.y);
        len = a;
    }
}
