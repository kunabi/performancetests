﻿using UnityEngine;
using System.Collections;


public class DotVector : PerformanceTester
{
    Vector2 A = new Vector2(2f,3f), B = new Vector2(4f,5f);
    public float dot;

    public override void Awake()
    {
        description = "100k 2d dot products";
        base.Awake();
    }

    public override void ProfileCode() 
    {
        float a = 0f;
        for (int i = 0; i < 100000; i++)
            a += Vector2.Dot(A,B);
        dot = a;
    }
}
