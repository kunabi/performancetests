﻿using UnityEngine;
using System.Collections;


public class FloatOperations : PerformanceTester
{
    float ax = 2f, ay = 3f, bx = 4f, by = 5f;
    public float cx = 0f, cy = 7f;

    public override void Awake()
    {
        formatString = "{0:N3} ms";
        description = "100k float Ops, 2 of each:\nAdd, Sub, Div, Mult";
        base.Awake();
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < 100000; i++)
        {
            cx += ax + bx;
            cy += ay + by;
            cx += ax - by;
            cy += ay - by;
            cx += ax / 3f;
            cy += ay / 3f;
            cx += bx * 4f;
            cy += by * 4f;
        }
    }
}
