﻿using UnityEngine;
using System.Collections;


public class VectorOperations : PerformanceTester
{
    Vector2 A = new Vector2(2f,3f), B = new Vector2(4f,5f);
    public Vector2 C = Vector2.zero;

    public override void Awake()
    {
        description = "100k Vector2 Ops:\nAdd, Sub, Div, Mult";
        base.Awake();
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < 100000; i++)
        {
            C += A+B;
            C += A-B;
            C += A / 3f;
            C += B * 4f;
        }
    }
}
