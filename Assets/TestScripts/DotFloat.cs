﻿using UnityEngine;
using System.Collections;


public class DotFloat : PerformanceTester
{
    float ax = 2f, ay = 3f, bx = 4f, by = 5f;
    public float dot;

    public override void Awake()
    {
        description = "100k float ops equivalent to Vector2.Dot";
        base.Awake();
    }

    public override void ProfileCode() 
    {
        float a = 0f;
        for (int i = 0; i < 100000; i++)
            a += ax * bx + ay * by;
        dot = a;
    }
}
