﻿using UnityEngine;
using System.Collections;


public class MultiComponentOps : PerformanceTester
{
    protected BaseComponent[] bcs;
    protected int bcsCount;

    public override void Awake()
    {
        bcs = BehaviourPool<BaseComponent>.Instance().pool;
        bcsCount = BehaviourPool<BaseComponent>.Instance().poolSize;
        base.Awake();
    }
}
