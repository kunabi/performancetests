﻿using UnityEngine;
using System.Collections;


public class LoopDistributedOpsExtWrite : PerformanceTester
{
    public float cx = 2f, cy = 3f;
    BaseComponent compA;
    DerivedComponent compB;

    public override void Awake()
    {
        description = "100k float Ops, 2 of each:\nAdd, Sub, Div, Mult\nDistributed over 3 components, and writing to one external component";
        base.Awake();
        compA = gameObject.AddComponent<BaseComponent>();
        compB = gameObject.AddComponent<DerivedComponent>();
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < 100000; i++)
        {
            cx -= compA.ax + compB.bx;
            cy -= compA.ay + compB.by;
            cx += compA.ax - compB.by;
            cy += compA.ay - compB.by;
            cx += compA.ax / 3f;
            cy += compA.ay / 3f;
            compA.ax = cx + compB.bx * 4f;
            compA.ay = cy + compB.by * 4f;
        }
    }
}
