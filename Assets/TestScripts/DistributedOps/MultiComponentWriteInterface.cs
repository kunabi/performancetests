﻿using UnityEngine;
using System.Collections;


public class MultiComponentWriteInterface : MultiComponentOps
{
    public override void Awake()
    {
        base.Awake();
        SetDescription(bcsCount + " objects, doing float additions with 2 local floats, storing result in another object via interface autoproperty");
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < bcsCount; i++)
            bcs[i].AddToRemoteViaInterface();
    }
}
