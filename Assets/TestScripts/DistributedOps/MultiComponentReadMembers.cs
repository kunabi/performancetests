﻿using UnityEngine;
using System.Collections;


public class MultiComponentReadMembers : MultiComponentOps
{
    public override void Awake()
    {
        base.Awake();
        SetDescription(bcsCount + " objects, doing float additions with 2 floats aquired from another object.");
    }

    public override void ProfileCode() 
    {
        for (int i = 0; i < bcsCount; i++)
            bcs[i].AddToLocal();
    }
}
